﻿using UnityEngine;
using System.Collections;
using SQLite4Unity3d;

public class usersalternative
{
	[PrimaryKey, AutoIncrement, NotNull]
	public int id { get; set; }
	[NotNull]
	public int highscore { get; set; }
	[NotNull]
	public string name { get; set; }
}
